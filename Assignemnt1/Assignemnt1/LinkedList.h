
template <typename T>

class LinkedList //Singel linked list
{
	struct Node
	{
		T value;
		Node* NextNode;
	};
public:
	Node* m_root;


	LinkedList()
	{
		m_root = nullptr;
	}
	~LinkedList()
	{
		Clear();
		delete m_root;
	}

	void PushBack(T value)
	{
		Node* node = new Node();

		node->value = value;

		Node* temp = m_root;
		if (temp != nullptr)
		{
			while (temp->NextNode != nullptr) 
			{
				temp = temp->NextNode; 
			}
			temp->NextNode = node;

		}
		else 
		{
			m_root = node;
		}
	}
	void PushFront(T value)
	{
		Node* node = new Node();

		node->value = value; 

		if (m_root != nullptr) 
		{
			node->NextNode = m_root;
			m_root = node;
		}
		else 
		{
			m_root = node;
		}
	}

	void PopFront()
	{
		if (m_root != nullptr)
		{
			Node* temp = m_root;
			m_root = m_root->NextNode;
			delete temp;
			temp = nullptr;
		}
	}

	void PopBack()
	{
		Node* temp = m_root;
		if (temp != nullptr)
		{
			if (temp->NextNode != nullptr)
				while (temp->NextNode->NextNode != nullptr) 
				{
					temp = temp->NextNode;
				}

			delete temp->NextNode;

			temp->NextNode = nullptr;


		}

	}

	void Clear()
	{
		while (m_root != nullptr)
			PopFront();
	}

	bool Find(T value)
	{
		Node* temp = m_root;
		while (temp != nullptr)
		{
			if (temp->value == value)
				return true;

			temp = temp->NextNode;
		}
		return false;
	}


	int Size()
	{
		int size = 0;
		if (m_root != nullptr)
		{
			size++;
			Node* temp = m_root;
			while (temp->NextNode != nullptr)
			{
				size++;
				temp = temp->NextNode;
			}
		}
		return size;
	}
};
