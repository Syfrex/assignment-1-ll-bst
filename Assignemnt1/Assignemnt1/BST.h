#pragma once
#include <iostream>
#include <vector>

enum Traversal
{
	PREORDER,
	INORDER,
	POSTORDER,
};


template <typename T>
class BST//
{

	struct Node
	{
		T value;
		Node* left;
		Node* right;

		Node(T m_value)
		{
			value = m_value;
			left = nullptr;
			right = nullptr;
		}
	};

	
	Node* m_root;

public:

	std::vector < T > TraversalVect;

BST()
{
	m_root = nullptr;
}


~BST()
{
	Clear();
}


void InsertNode(const T &m_value)
{
		Insert(m_value, m_root);
}

void Clear()
{
	DeleteTree(m_root);

	m_root = nullptr;
}



bool Find(const T &m_value)
{
		return NodeWithValueExists(m_root, m_value);
}



int Size()
{
	unsigned int NodeValue = 0;
	SizeNode(m_root, NodeValue);
	return NodeValue;
}

void PrintAndSave(Traversal PrintOrder)
{
	TraversalVect.clear();
	if (PrintOrder == PREORDER)
		TraversalPreOrder();
	else if (PrintOrder == POSTORDER)
		TraversalPostOrder();
	else
		TraversalInOrder();
}


private: 

void Insert(const T &m_value, Node* node)
{
	if (m_root == nullptr)
	{
		m_root = new Node(m_value);
	}
	else if (m_value < node->value)
	{
		if (node->left != nullptr)
		{
			Insert(m_value, node->left);
		}
		else
		{
			node->left = new Node(m_value);
		}
	}
	else if (m_value > node->value)
	{
		if (node->right != nullptr)
		{
			Insert(m_value, node->right);
		}
		else
		{
			node->right = new Node(m_value);
		}
	}
	else
	{
		std::cout << "The node " << node << " has already been added to the tree." << std::endl;
	}
}

bool NodeWithValueExists(Node* m_startSearchNode, const T &m_value)
{
	if (m_startSearchNode != nullptr)
	{
		if (m_startSearchNode->value == m_value)
		{
			return true;
		}
		else if (m_value < m_startSearchNode->value)
		{
			NodeWithValueExists(m_startSearchNode->left, m_value);

		}
		else if (m_value > m_startSearchNode->value)
		{
			NodeWithValueExists(m_startSearchNode->right, m_value);
		}
		else
		{
			return false;
		}
	}
}

void SizeNode(Node* node, unsigned int &NodeValue)
{
	if (node != nullptr)
	{
		SizeNode(node->left, NodeValue);
		SizeNode(node->right, NodeValue);
		NodeValue++;
	}
}

void DeleteTree(Node* node)
{
	if (node != nullptr)
	{
		if (node->left != nullptr)
		{
			DeleteTree(node->left);
		}
		if (node->right != nullptr)
		{
			DeleteTree(node->right);
		}
		delete node;
		node = nullptr;
	}
}

void TraversalPreOrder(){
	std::cout << "List in PRE-ORDER Traversal: " << std::endl;
	TraversalPreOrder(m_root);
}


void TraversalPreOrder(Node* node)
{
	if (node != nullptr)
	{
		std::cout << node->value << ", ";
		TraversalVect.push_back(node->value);
		TraversalPreOrder(node->left);
		TraversalPreOrder(node->right);
	}
}


void TraversalInOrder()
{
	std::cout << "List in IN-ORDER Traversal: " << std::endl;
	TraversalInOrder(m_root);
}

void TraversalInOrder(Node* node)
{
	if (node != nullptr)
	{
		TraversalInOrder(node->left);
		std::cout << node->value << ", ";
		TraversalVect.push_back(node->value);
		TraversalInOrder(node->right);

	}
}

void TraversalPostOrder()
{
	std::cout <<  "List in POST-ORDER Traversal: " << std::endl;
	TraversalPostOrder(m_root);
}


void TraversalPostOrder(Node* node)
{
	if (node != nullptr)
	{
		TraversalPostOrder(node->left);
		TraversalPostOrder(node->right);
		std::cout << node->value << ", ";
		TraversalVect.push_back(node->value);
	}
}
};