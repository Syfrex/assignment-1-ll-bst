#include <iostream>
#include <array>
#include "LinkedList.h"
#include "BST.h"
#include "unit.hpp"




int main(int argc, char* argv[])
{
	LinkedList<int> List;
	BST <int> BinTree;
	int Unit_test_Traversal[6];
	bool verify_true = false;

	//------ LINKEDLIST (SINGLE) ------------------------------------------------------------
	//"PUSHFRONT"
	List.PushFront(2);
	List.PushFront(6);
	List.PushFront(7);
	//"PUSHBACK"
	List.PushBack(3);
	List.PushBack(10);

	// "FIND"
	std::cout << "Testing 'Find' LinkedList: " << std::endl;
	verify(true, List.Find(6), "Test '1' Found");
	std::cout<<std::endl;
	verify(true, List.Find(3), "Test '2' Found");
	std::cout << std::endl;

	// "SIZE"
	std::cout << "Testing 'Size' LinkedList: " << std::endl;
	verify(5, List.Size(), "The size before removing any nodes: ");
	std::cout << List.Size();

	std::cout << std::endl;
	std::cout << std::endl;//Make the list more read-able in the consol

	//"POPFRONT"
	List.PopFront();
	std::cout << "Testing 'Popfront' LinkedList: " << std::endl;

	verify(false, List.Find(7), "Could not find first node, remove it was a success");
	std::cout << std::endl;
	//"POPBACK"
	List.PopBack();
	std::cout << "Testing 'Popback' LinkedList: " << std::endl;

	verify(false, List.Find(10), "Could not find back node, remove it was a success");
	std::cout << std::endl;

	// "SIZE"
	verify(3, List.Size(), "The size after removing nodes");
	std::cout << List.Size();
	std::cout << std::endl;
	std::cout << std::endl;
	//"CLEAR"
	std::cout << "Testing 'Clear' LinkedList: " << std::endl;

	List.Clear();
	verify(0, List.Size(), "The list is now empty");
	std::cout << std::endl;
	//----------END OF LINKEDLIST (SINGLE)-------------------------------------------------------
	//----------BINARY SEARCHTREE-------------------------------------------------------
	std::cout << std::endl;

	std::cout << "BINARYSEARCHTREE: ";
	std::cout << std::endl;

	//"Insert"
	std::cout << "Testing 'Insert(10,2,9,19,15,5)' BST: " << std::endl;

	BinTree.InsertNode(10);
	Unit_test_Traversal[0] = 10;
	BinTree.InsertNode(2);
	Unit_test_Traversal[1] = 2;
	BinTree.InsertNode(9);
	Unit_test_Traversal[2] = 9;
	BinTree.InsertNode(19);
	Unit_test_Traversal[3] = 19;
	BinTree.InsertNode(15);
	Unit_test_Traversal[4] = 15;
	BinTree.InsertNode(5);
	Unit_test_Traversal[5] = 5;

	//"Find"
	std::cout << "Testing 'Find (5 and 10)' BST: " << std::endl;

	verify(true, BinTree.Find(5), "Found the node");
	verify(true, BinTree.Find(10), "Found the other node");
	std::cout << std::endl;
	//"Traversal"
	std::cout << "Testing traversal in order: ";
	BinTree.PrintAndSave(Traversal::INORDER);
	Unit_test_Traversal[0] = 2;
	Unit_test_Traversal[1] = 5;
	Unit_test_Traversal[2] = 9;
	Unit_test_Traversal[3] = 10;
	Unit_test_Traversal[4] = 15;
	Unit_test_Traversal[5] = 19;
	std::cout << std::endl;

	for (int i = 0; i < BinTree.TraversalVect.size(); i++)
	{
		verify(BinTree.TraversalVect[i], Unit_test_Traversal[i], "The number is INORDER");
	}
	std::cout << std::endl;
	std::cout << "Testing traversal pre order: ";
	BinTree.PrintAndSave(Traversal::PREORDER);
	Unit_test_Traversal[0] = 10;
	Unit_test_Traversal[1] = 2;
	Unit_test_Traversal[2] = 9;
	Unit_test_Traversal[3] = 5;
	Unit_test_Traversal[4] = 19;
	Unit_test_Traversal[5] = 15;
	std::cout << std::endl;

	for (int i = 0; i < BinTree.TraversalVect.size(); i++)
	{
		verify(BinTree.TraversalVect[i], Unit_test_Traversal[i], "The number is PREORDER");
	}
	std::cout << std::endl;
	std::cout << "Testing traversal post order: ";
	BinTree.PrintAndSave(Traversal::POSTORDER);
	Unit_test_Traversal[0] = 5;
	Unit_test_Traversal[1] = 9;
	Unit_test_Traversal[2] = 2;
	Unit_test_Traversal[3] = 15;
	Unit_test_Traversal[4] = 19;
	Unit_test_Traversal[5] = 10;
	std::cout << std::endl;

	for (int i = 0; i < BinTree.TraversalVect.size(); i++)
	{
		verify(BinTree.TraversalVect[i], Unit_test_Traversal[i], "The number is POSTORDER");
	}
	std::cout << std::endl;
	//"Size"
	std::cout << "Testing 'Size (6 nodes)' BST: " << std::endl;

	verify(6, BinTree.Size(), "Size is correct");
	std::cout << std::endl;
	//"CLEAR"
	std::cout << "Testing 'Clear (size 0 nodes)' BST: " << std::endl;

	BinTree.Clear();
	verify(0, BinTree.Size(), "The tree is cleard");

	//----------END OF BINARY SEARCHTREE-------------------------------------------------------






	std::cin.get();


	return 0;




}